package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

public class RPNCalculator {

    private Map<String, OperationStack> map;

    public float eval(String expression) {

        CheckIllegalArgumentExpression(expression);
        this.createOperators();
        LinkedList<Float> stack = new LinkedList<Float>();

        for (String token : expression.split("\\s")) {
            Float num = null;
            try {
                num = Float.parseFloat(token);

            } catch (NumberFormatException e) {
            }
            if (num != null) {
                stack.push(Float.parseFloat(token + ""));
            } else {
                CheckIllegalArguments(stack, token);
                map.get(token).calculate(stack);
            }
        }
        return stack.pop();
    }

    private void CheckIllegalArgumentExpression(String expression) {
        Optional.ofNullable(expression).orElseThrow(() -> new IllegalArgumentException());
    }

    private void CheckIllegalArguments(LinkedList<Float> stack, String token) {
        Optional.of(stack.size()).filter(i -> i >= 1).orElseThrow(() -> new IllegalArgumentException());
        Optional.ofNullable(map.get(token)).orElseThrow(() -> new IllegalArgumentException());
    }

    private void createOperators() {
        this.map = new HashMap<String, OperationStack>();

        map.put("+", new AddStack());
        map.put("-", new SubstractionStack());
        map.put("", new EmptyStack());
        map.put("/", new DivisionStack());
        map.put("*", new ProductStack());
        map.put("MOD", new ModStack());
        map.put("**", new ProductMultipleStack());
        map.put("//", new DivisionMultipleStack());
        map.put("++", new MultiAddStack());
        map.put("--", new SubstractionMultipleStack());

    }
}