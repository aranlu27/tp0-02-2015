package ar.fiuba.tdd.tp0;

import java.util.*;

class ProductMultipleStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        OperationStack productoStack = new ProductStack();
        ItStack.iterate(stack, productoStack);
    }
}

class DivisionMultipleStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        OperationStack divisionReverse = new ReverseDivisionStack();
        ItStack.iterate(stack, divisionReverse);
    }
}

class MultiAddStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        OperationStack addStack = new AddStack();
        ItStack.iterate(stack, addStack);
    }
}

class SubstractionMultipleStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        OperationStack substractionReverse = new ReverseSubstractionStack();
        ItStack.iterate(stack, substractionReverse);
    }
}

class ItStack {
    public static void iterate(LinkedList<Float> stack, OperationStack op) {
        int opNum = stack.size() - 1;
        for (int i = 0; i < opNum; i++) {
            op.calculate(stack);
        }
    }
}

