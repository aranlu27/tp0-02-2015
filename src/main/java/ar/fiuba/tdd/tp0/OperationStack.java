package ar.fiuba.tdd.tp0;

import java.util.LinkedList;
import java.util.Optional;

interface OperationStack {
    void calculate(LinkedList<Float> stack);
}

class AddStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        Optional.of(stack.size()).filter(i -> i >= 2).orElseThrow(() -> new IllegalArgumentException());
        float operation2 = stack.pop();
        float operation1 = stack.pop();
        stack.push(operation1 + operation2);
    }
}

class SubstractionStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        float subs_term2 = stack.pop();
        float subs_term1 = stack.pop();
        stack.push(subs_term1 - subs_term2);
    }
}

class ProductStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        float operation2 = stack.pop();
        float operation1 = stack.pop();
        stack.push(operation1 * operation2);
    }
}

class EmptyStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        throw new IllegalArgumentException();
    }

    public OperationStack cloan() {
        return new EmptyStack();
    }
}

class DivisionStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        float secondOperand = stack.pop();
        float firstOperand = stack.pop();
        stack.push(firstOperand / secondOperand);
    }

    public OperationStack cloan() {
        return new DivisionStack();
    }
}

class ReverseDivisionStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        stack.push(stack.pop() / stack.pop());
    }
}

class ReverseSubstractionStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        stack.push((stack.pop()) + (stack.pop() * -1));
    }
}

class ModStack implements OperationStack {
    public void calculate(LinkedList<Float> stack) {
        float op2 = stack.pop();
        float op1 = stack.pop();
        stack.push(op1 % op2);
    }
}
